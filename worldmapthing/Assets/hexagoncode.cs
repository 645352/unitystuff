﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hexagoncode : MonoBehaviour {

    // 3 bools that will be is for each color that the tile can be
    // changes tag based on wich color it is
    // check how many friendly(blue) it got close by disabling and activating the collider. that adds 1 to "number_of_close_friendly" or -1 if it exits. reset the whole checking procces with refreshcollider()
    // when there are more then 1 blue close and the unit is an enemy it can be attacked

    public bool red;
     public bool grey;
     public bool blue;

    public GameObject red_ui;
    public GameObject grey_ui;
    public GameObject blue_ui;

    public GameObject red_sprite;
    public GameObject grey_sprite;
    public GameObject blue_sprite;
    public CircleCollider2D circl2d;
    bool attackable;
    int numbe_of_close_friendly;
    private void Start()
    {
        refreshcollider();

        attackable_check();
        change_sprite();
        changetag();
        print(gameObject.name + "friendly close" + numbe_of_close_friendly);
    }
    void Update () {

        attackable_check();
        change_sprite();
        changetag();
        print(gameObject.name + "friendly close" + numbe_of_close_friendly);
       
    }
    void change_sprite()
    {
        if (red == true)
        {
            disableall_sprites();
            red_sprite.SetActive(true);
          
        }
        if (blue == true)
        {
            disableall_sprites();
            blue_sprite.SetActive(true);
        }
        if (grey == true)
        {
            disableall_sprites();
            grey_sprite.SetActive(true);
        }
    }
    
    public void activate_ui(GameObject gob)
    {
        disableall_ui();
        gob.SetActive(true);
    }

    void disableall_sprites()
    {
        red_sprite.SetActive(false);
        grey_sprite.SetActive(false);
        blue_sprite.SetActive(false);
    }

    void disableall_ui()
    {
        red_ui.SetActive(false);
        grey_ui.SetActive(false);
        blue_ui.SetActive(false);
    }
    void refreshcollider()
    {
        colideractiv(false);
        numbe_of_close_friendly = 0;
        colideractiv(true);

    }
    void colideractiv(bool a)
    {
     
        circl2d.enabled = a;
        
    }

    public void close_ui()
    {
        disableall_ui();
        

    }
    //when a friendly is close it adds to a int that keeps track if it can be attacked or not and changes bool attackable when there is 0 friendly units close
    void attackable_check()
    {
        if (numbe_of_close_friendly >= 1)
        {
            attackable = true;
            print("red tile can be attacked " + attackable);
        }
        else
        {
            attackable = false;
            print("red tile can be attacked " + attackable);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //only relevent when checking if it can be attacked. aka the red tile
        if (collision.tag == "blue")
        {

            numbe_of_close_friendly++;
            
        }
        print(collision.name + " entered " + gameObject.name);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "blue")
        {
            numbe_of_close_friendly--;
        }
        print(collision.name + " entered " + gameObject.name);
    }
    public void when_clicked()
    {
        //to refresh triggerenter and exit

        refreshcollider();
        if (red == true)
        {
            if (attackable == true)
            {
          
                activate_ui(red_ui);
            }
        }
        else if (blue_ui)
        {
      
            activate_ui(blue_ui);
        }
        else if (grey == true)
        {
           
            activate_ui(grey_ui);
        }
    }
    public void changetag()
    {
        if (blue == true)
        {
            gameObject.tag = "blue";
        }
        if (blue == false)
        {
            gameObject.tag = "red";
        }
        if (grey == true)
        {
            gameObject.tag = "grey";
        }
    }
}
