﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScenes : MonoBehaviour {

    //code will change scene and show a loading bar.
    //created 2017-12-06 stenhenriksson
    //loadingscreen will show a procent progression of the loading
    public int scenenumber = 0;
    public GameObject loadingScreen;
    private void Update()
    {
        
    }

    //loadlevel gets called by button to load the scene
    public void loadlevel(int sceneIndex)
    {

        StartCoroutine(LoadAsynchronosly(sceneIndex));
    }
    IEnumerator LoadAsynchronosly(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while (operation.isDone == false)
        {
            Debug.Log(operation.progress);
            yield return null;
        }
    }
}
