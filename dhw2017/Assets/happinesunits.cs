﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class happinesunits : MonoBehaviour {
    public int food;
    public int maxfood;
    public int population;

    public int dough;
    public int meat;
    public int chees; 
    void Start () {

        population = PlayerPrefs.GetInt("population");

        dough = PlayerPrefs.GetInt("dough");
        meat = PlayerPrefs.GetInt("meat");
        chees = PlayerPrefs.GetInt("chees");
        food = PlayerPrefs.GetInt("food");
        maxfood = PlayerPrefs.GetInt("maxfood"); 

        PlayerPrefs.SetInt("maxfood", maxfood + 3);

    }
	
	// Update is called once per frame
	void Update () {
        population = PlayerPrefs.GetInt("population");

        dough = PlayerPrefs.GetInt("dough");
        meat = PlayerPrefs.GetInt("meat");
        chees = PlayerPrefs.GetInt("chees");
        food = PlayerPrefs.GetInt("food");
        maxfood = PlayerPrefs.GetInt("maxfood");
    }
}
