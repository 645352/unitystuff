﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generatemap : MonoBehaviour {
    public float sizex;
    public float sizey;
    public GameObject tiletospawn;
    
    	// Use this for initialization
	void Start () {
        for (float x = 1; x < sizex - 1; x++)
        {
            //Within each column, loop through y axis (rows).
            for (float y = 1; y < sizey - 1; y++)
            {
                Instantiate(tiletospawn, new Vector3(x,y, 0), Quaternion.identity);

            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
