﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttontimer : MonoBehaviour
{
    //timerscript that activates clickable gameobject
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "grid")
        {
            Destroy(collision.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
    }
    public float start_time;
    public float refreshtimer;
    public int resourcegain;
    public string resourcename;
    public GameObject tile;
    public GameObject ui;
    public GameObject clickable;
    public int food;
    public int maxfood;
    public int population;

    public int dough;
    public int meat;
    public int chees;

    public GameObject tiletospawn;
    // Use this for initialization
    void Start()
    {
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x-1, gameObject.transform.position.y + 1, 0), Quaternion.identity);
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x , gameObject.transform.position.y + 1, 0), Quaternion.identity);
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x +1, gameObject.transform.position.y + 1, 0), Quaternion.identity);

        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y , 0), Quaternion.identity);
       
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y , 0), Quaternion.identity);


        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y - 1, 0), Quaternion.identity);
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, 0), Quaternion.identity);
        Instantiate(tiletospawn, new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y - 1, 0), Quaternion.identity);


    }

    //refreshes time
    void Update()
    {
        population = PlayerPrefs.GetInt("population");

        dough = PlayerPrefs.GetInt("dough");
        meat = PlayerPrefs.GetInt("meat");
        chees = PlayerPrefs.GetInt("cheez");
        start_time -= Time.deltaTime;
        if (start_time <= 0)
        {
            excecutetimecode();

            //resets the timer
         
        }
    }
    //gets called everytime starttime<=0
    public void excecutetimecode()
    {

        ui.SetActive(true);
        clickable.SetActive(true);








    }
    //add to resources
    public void addresources()
    {
        print("recourcesclicked");
        start_time = refreshtimer;
        int b = PlayerPrefs.GetInt(resourcename);
        print("resorucegain" + resourcename + " " + b);
       
        PlayerPrefs.SetInt(resourcename, b + 1);
        ui.SetActive(false);
        clickable.SetActive(false);
    }

}
