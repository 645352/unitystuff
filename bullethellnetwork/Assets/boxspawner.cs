﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxspawner : MonoBehaviour {
    public string spawningstring;
    public GameObject spawner1;
    public GameObject spawner2;
    public GameObject spawner3;
    public GameObject spawner4;
    public int maxnumberofboxes;
    public float timeLeft;
    public float timeLeft1;
    int boxes;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {

            if (boxes < maxnumberofboxes)
            {
                spawn();
            }
         
            timeLeft = timeLeft1;
        }
    }
    public void spawn()
    {
        if (PhotonNetwork.isMasterClient)
        {
            {
                float random = Random.Range(1, 5);
                print("random:" + random);


                if (random == 1)
                {
                    GameObject monster = PhotonNetwork.Instantiate(spawningstring, spawner1.transform.position, Quaternion.identity, 0);
                }
                if (random == 2)
                {
                    GameObject monster = PhotonNetwork.Instantiate(spawningstring, spawner2.transform.position, Quaternion.identity, 0);
                }
                if (random == 3)
                {
                    GameObject monster = PhotonNetwork.Instantiate(spawningstring, spawner3.transform.position, Quaternion.identity, 0);
                }
                if (random == 4)
                {
                    GameObject monster = PhotonNetwork.Instantiate(spawningstring, spawner4.transform.position, Quaternion.identity, 0);
                }
            }
        }
            
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "box")
        {
            boxes++;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "box")
        {
            boxes--;
        }
    }
}
