﻿using GameSparks.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class registernewplayer : MonoBehaviour {
    public string input1;
    public string input2;
    public GameObject ui;
    public GameObject characets;
    public Text character1class;
    public Text character1exp;
    

    public Text character2class;
    public Text character2exp;
  
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

    }

    public void getstring(string input1a)
    {
        input1 = input1a;
        print(input1a);

    }
    public void getstring2(string input2a)
    {
        input2 = input2a;
        print(input2a);
    }
    public void registernewplayers()
    {
        print("rigisternewplayerpressed");
        string a ="asdd";
        string b="asdad";
        a = input1;
        b = input2;
                new GameSparks.Api.Requests.RegistrationRequest()
           .SetDisplayName(a)
           .SetPassword("asdasdadd")
           .SetUserName(b)
           .Send((response) => {
               if (!response.HasErrors)
               {
                   Debug.Log("Player Registered");
                   authenicateplayer();
               }
               else
               {
                   Debug.Log("Error Registering Player");
                   print(a);
                   print(b);
               }
           }
         );

    }
    public void addgoods()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("SAVE_PLAYER").SetEventAttribute("XP", "123456").SetEventAttribute("GOLD", "100").SetEventAttribute("ATTACK", 100).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Player Saved To GameSparks...");
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void addtostuff()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1").SetEventAttribute("CLASS", 3).SetEventAttribute("CHEST", 3).SetEventAttribute("WEPON", 3).SetEventAttribute("HEAD", 3).SetEventAttribute("EXP", 3).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void deviceauthenication()
    {
        new GameSparks.Api.Requests.DeviceAuthenticationRequest().SetDisplayName("Randy").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Device Authenticated...");
            }
            else
            {
                Debug.Log("Error Authenticating Device...");
            }
        });
    }
    
    public void loadchar1()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1LOAD").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");
                GSData data = response.ScriptData.GetGSData("Char1DATA");

                print("Player playerID: " + data.GetInt("playerClass"));



            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });

    }
    public void loadchar2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2LOAD").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");
                GSData data1 = response.ScriptData.GetGSData("Char2DATA");

                
                var a = data1.GetString("playerClass");
                print("char2class " + a);


            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });

    }
    public void authenicateplayer()
    {
        new GameSparks.Api.Requests.AuthenticationRequest().SetUserName(input2).SetPassword("asdasdadd").Send((response) => {
            if (!response.HasErrors)
            {  ui.SetActive(false);
                characets.SetActive(true);
                addtostuff();

                loadchar2();
                Debug.Log("Player Authenticated...");
                
            }
            else
            {
              
                Debug.Log("Error Authenticating Player...");
            }
        });
    }   
    public void getdata()
    {
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");
                GSData data = response.ScriptData.GetGSData("player_Data");
                print("Player playerID: " + data.GetString("playerID"));
                print("Player playerXP: " + data.GetString("playerXP"));
                print("Player playerGold: " + data.GetString("playerGold"));
                print("Player playerAttack: " + data.GetInt("playerAttack"));
            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });
    }
}
