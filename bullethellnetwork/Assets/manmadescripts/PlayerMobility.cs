﻿using GameSparks.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMobility : Photon.PunBehaviour
{

    public static GameObject LocalPlayerInstance;
    public Rigidbody2D rigibBody;
    public float speed;
    Animator animator;

    
    float level;
    float health;
    float attack;
    float defense;
    float levell;
    string playerclass = " ";
    public Text text;
    private void Awake()
    {
        if (photonView.isMine)
        {
            LocalPlayerInstance = gameObject;
        }
    }
    void Start()
    {
        animator = GetComponent<Animator>();
        rigibBody = GetComponent<Rigidbody2D>();
        updateartibutes();
    }


    void FixedUpdate()
    {
        text.text = "" + health;
        if (photonView.isMine)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
            rigibBody.AddForce(transform.up * speed * Input.GetAxis("Vertical"));
            bool fire = Input.GetButtonDown("Fire1");
            print(fire);
            animator.SetBool("asd", fire);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "sword")
        {

            if (photonView.isMine)
            {
                health = health - 10;
                print("hitttttttttttttttttttttt");
                if (health < 9)
                {
                    PhotonNetwork.Disconnect();
                    PhotonNetwork.LeaveRoom();
                    Application.Quit();
                }
            }

        }

    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(health);



        }
        else
        {
            // Network player, receive data
            this.health = (int)stream.ReceiveNext();


        }
    }
    public void updateartibutes()
    {
        string aasdasd;
        int currentcharacter = PlayerPrefs.GetInt("selectedcharcter");
        if (photonView.isMine)
        {
            if (currentcharacter == 1)
            {
                aasdasd = "CHAR1LOAD";
            }
            else
            {
                aasdasd = "CHAR2LOAD";
            }
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey(aasdasd).Send((response) => {
                if (!response.HasErrors)
                {


                    Debug.Log("Received Player Data From GameSparks...");
                    GSData data = response.ScriptData.GetGSData("Char" + currentcharacter + "DATA");


                    var b = data.GetFloat("playerClass");
                    var c = data.GetFloat("playerXP");
                    var head = data.GetFloat("playerHead");
                    var chest = data.GetFloat("playerChest");
                    var wepon = data.GetFloat("playerWepon");
                    print("playerclass " + b + "playerxp " + c + "playerchest " + chest);
                    health = 0;
                    attack = 0;
                    defense = 0;
                    levell = 0;
                    print("Playerclassb: " + b);
                    //generates the class
                    if (b == 1)
                    {
                        playerclass = "warrior";
                        health = health + 3;
                    }
                    else if (b == 2)
                    {
                        attack = attack + 2;
                        playerclass = "wizzard";
                    }
                    else if (b == 3)
                    {
                        defense = defense + 3;

                        playerclass = "ranger";
                    }
                    else
                    {
                        playerclass = "dunno";
                    }
                    //generates the level
                    if (c <= 1)
                    {
                        level = 1;
                        attack = attack + 1;
                        defense = defense + 1;
                        health = health + 1;
                    }
                    if (c > 1 && c < 3)
                    {
                        levell = 2;
                        attack = attack + 2;
                        defense = defense + 2;
                        health = health + 2;
                    }
                    if (c > 3 && c < 6)
                    {
                        attack = attack + 3;
                        defense = defense + 3;
                        health = health + 3;
                        levell = 3;
                    }
                    if (c > 6 && c < 10)
                    {
                        attack = attack + 4;
                        defense = defense + 4;
                        health = health + 4;
                        levell = 4;
                    }
                    if (c > 10)
                    {
                        levell = 5;
                        attack = attack + 5;
                        defense = defense + 5;
                        health = health + 5;
                    }
                    else
                    {
                        levell = 99999;
                    }
                    //get item
                    if (head == 1)
                    {
                        defense = defense + 2;
                    }
                    else if (head == 2)
                    {

                        defense = defense + 3;
                    }
                    else if (head == 3)
                    {
                        defense = defense + 4;
                    }
                    else if (head == 4)
                    {
                        defense = defense + 5;
                    }
                    else if (head == 5)
                    {
                        defense = defense + 6;
                    }




                    if (wepon == 1)
                    {
                        attack = attack + 1;
                    }
                    else if (wepon == 2)
                    {
                        attack = attack + 2;
                    }
                    else if (wepon == 3)
                    {
                        attack = attack + 3;
                    }
                    else if (wepon == 4)
                    {
                        attack = attack + 4;
                    }
                    else if (wepon == 5)
                    {
                        attack = attack + 5;
                    }

                    if (chest == 1)
                    {
                        defense = defense + 3;
                    }
                    else if (chest == 2)
                    {
                        defense = defense + 4;
                    }
                    else if (chest == 3)
                    {
                        defense = defense + 5;
                    }
                    else if (chest == 4)
                    {
                        defense = defense + 6;
                    }
                    else if (chest == 5)
                    {
                        defense = defense + 7;
                    }




                }
                else
                {
                    Debug.Log("Error Loading Player Data...");
                }
            });
        }
    }
    public void characterwhipe()
    {
        int currentcharacter = PlayerPrefs.GetInt("selectedcharcter");
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR" + currentcharacter).SetEventAttribute("CLASS", 0).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 0).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
}
