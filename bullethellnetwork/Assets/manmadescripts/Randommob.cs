﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Randommob :Photon.PunBehaviour,IPunObservable {
    Animator animator;
    Rigidbody2D rigibBody;
    public float timeLeft1;
    public float timeLeft12;
    float timeLeft;
    float timeLeft2;
    bool trigger = true;
    bool trigger2 = true;
    public float health = 5;
    public float attack =1;
    public float experiance=1;
    public Text hp;
   
    public float hptaken = 0;
    public string animationparameter;
    public GameObject xpzone;
    // Use this for initialization
    void Start () {
        timeLeft = timeLeft1;
        animator = GetComponent<Animator>();
        rigibBody = GetComponent<Rigidbody2D>();
        trigger = !trigger;
        animator.SetBool(animationparameter, trigger);
    }
	
	// Update is called once per frame
	void Update () {
        hp.text = "" + health;
        if (photonView.isMine)
        {
                timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                trigger = !trigger;
                animator.SetBool(animationparameter, trigger);
            
                timeLeft = timeLeft1;
            }

        }


    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "playersword")
        {

            if (photonView.isMine)
            {
                float a;
                print(collision);
                charactermovment mything = collision.gameObject.GetComponentInParent<charactermovment>();
                a = mything.attack;
                print("hit with " + a + "got" + health);
                health = health - a;
               
                if (health <= 0)
                {
                    xpzone.SetActive(true);
                    PhotonNetwork.Destroy(this.gameObject);
                }

            }

        }

    }
    public float xp = 15;
    public float xn = -15;
    public float yp = 0 ;
    public float yn = 0 ;
    private void FixedUpdate()
    {
        if (photonView.isMine)
        {
            timeLeft2 -= Time.deltaTime;
            if (timeLeft2 < 0)
            {
                trigger2 = !trigger2;
                if (trigger2 == true)
                {
                    rigibBody.velocity = new Vector2(xp, yp);
                }
                else
                {
                    rigibBody.velocity = new Vector2(xn, yn);
                }

                timeLeft2 = timeLeft12;
            }
        }
       
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {

            stream.SendNext(health);
            stream.SendNext(attack);
}
        else
        {
            this.attack = (float)stream.ReceiveNext();
            this.health = (float)stream.ReceiveNext();

        }
    }
}
