﻿using GameSparks.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class logincode : MonoBehaviour {
    public Text character1class;
    public Text character1exp;
    public Text character2class;
    public Text character2exp;
    public string input1;
    public string input2;
    public GameObject ui;
    public GameObject characets;
    public GameObject creatcharacter1;
    public GameObject creatcharacter2;
    public void authenicateplayer()
    {
        print("rigisternewplayerpressed");
        new GameSparks.Api.Requests.AuthenticationRequest().SetUserName(input2).SetPassword("asdasdadd").Send((response) => {
            if (!response.HasErrors)
            {
                ui.SetActive(false);
                characets.SetActive(true);
                addtostuff();
                loadchar1();
                loadchar2();
                Debug.Log("Player Authenticated...");
                addtostuff();
                addtostuff2();
            }
            else
            {

                Debug.Log("Error Authenticating Player...");
            }
        });
    }
    public void registernewplayers()
    {
        print("rigisternewplayerpressed");
        string a = "asdd";
        string b = "asdad";
        a = input1;
        b = input2;
        new GameSparks.Api.Requests.RegistrationRequest()
   .SetDisplayName(a)
   .SetPassword("asdasdadd")
   .SetUserName(b)
   .Send((response) => {
       if (!response.HasErrors)
       {
           Debug.Log("Player Registered");
           authenicateplayer();
       }
       else
       {
           Debug.Log("Error Registering Player");
           print(a);
           print(b);
       }
   }
 );

    }
    public void getstring(string input1a)
    {
        input1 = input1a;
        print(input1a);

    }
    public void getstring2(string input2a)
    {
        input2 = input2a;
        print(input2a);
    }
    public void loadchar1()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1LOAD").Send((response) => {
            if (!response.HasErrors)
            {
                int levell=0;
                string playerclass= " ";
                Debug.Log("Received Player Data From GameSparks...");
                GSData data = response.ScriptData.GetGSData("Char1DATA");
             
                var b = data.GetFloat("playerClass");
                var c = data.GetFloat("playerXP");
               
                print("Playerclassb: " + b);
               //generates the class
                if(b == 1)
                {
                    playerclass = "warrior";
                }
                else if (b == 2)
                {
                    playerclass = "wizzard";
                }
                else if (b == 3)
                {
                    playerclass = "ranger";
                }
                else
                {
                    playerclass = "dunno";
                }
               //generates the level
               if(c<=1)
                {
                    levell = 1;
                }
                if (c > 1&&  c < 3)
                {
                    levell = 2;
                }
                if (c > 3 && c < 6)
                {
                    levell = 3;
                }
                if (c > 6 && c < 10)
                {
                    levell = 4;
                }
                if (c > 10 )
                {
                    levell = 5;
                }
                else
                {
                    levell = 99999;
                }



                character1class.text = "class " + playerclass;
                character1exp.text = "level " + levell;


            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });

    }
    public void addtostuff()
    {

        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1").SetEventAttribute("CLASS", 3).SetEventAttribute("CHEST", 3).SetEventAttribute("WEPON", 3).SetEventAttribute("HEAD", 3).SetEventAttribute("EXP", 3).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void addtostuff2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2").SetEventAttribute("CLASS", 3).SetEventAttribute("CHEST", 3).SetEventAttribute("WEPON", 3).SetEventAttribute("HEAD", 3).SetEventAttribute("EXP", 3).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void loadchar2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2LOAD").Send((response) => {
            if (!response.HasErrors)
            {
                int levell = 0;
                string playerclass = " ";
                Debug.Log("Received Player Data From GameSparks...");
                GSData data = response.ScriptData.GetGSData("Char2DATA");

                var b = data.GetFloat("playerClass");
                var c = data.GetFloat("playerXP");

                print("Playerclassb: " + b);
                //generates the class
                if (b == 1)
                {
                    playerclass = "warrior";
                }
                else if (b == 2)
                {
                    playerclass = "wizzard";
                }
                else if (b == 3)
                {
                    playerclass = "ranger";
                }
                else
                {
                    playerclass = "dunno";
                }
                //generates the level
                if (c <= 1)
                {
                    levell = 1;
                }
                else if(c > 1 && c < 3)
                {
                    levell = 2;
                }
                else if(c > 3 && c < 6)
                {
                    levell = 3;
                }
                else if(c > 6 && c < 10)
                {
                    levell = 4;
                }
                else if(c > 10)
                {
                    levell = 5;
                }
                else
                {
                    levell = 99999;
                }



                character2class.text = "class " + playerclass;
                character2exp.text = "level " + levell;


            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });

    }
    public void creatcharacterwarrior1()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1").SetEventAttribute("CLASS", 1).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void creatcharacterwizzard1()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1").SetEventAttribute("CLASS", 2).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void creatcharacterranger1()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR1").SetEventAttribute("CLASS", 3).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void creatcharacterwarrior2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2").SetEventAttribute("CLASS", 1).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void creatcharacterwizzard2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2").SetEventAttribute("CLASS", 2).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void creatcharacterranger2()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("CHAR2").SetEventAttribute("CLASS", 3).SetEventAttribute("CHEST", 0).SetEventAttribute("WEPON", 0).SetEventAttribute("HEAD", 0).SetEventAttribute("EXP", 1).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("char1saved");
                creatcharacter1.SetActive(false);
                creatcharacter2.SetActive(false);
                characets.SetActive(true);
                loadchar1();
                loadchar2();
            }
            else
            {
                Debug.Log("Error Saving Player Data...");
            }
        });
    }
    public void changetochracterselect1() {
        ui.SetActive(false);
        characets.SetActive(false);
        creatcharacter1.SetActive(true);
        creatcharacter2.SetActive(false);
        loadchar1();
        loadchar2();



    }
    public void changetochracterselect2()
    {
        loadchar1();
        loadchar2();
        ui.SetActive(false);
        characets.SetActive(false);
        creatcharacter2.SetActive(true);
        creatcharacter1.SetActive(false);


    }
    public void gobacktocharacterselect()
    {
        ui.SetActive(false);
        characets.SetActive(true);
        creatcharacter1.SetActive(false);
        creatcharacter2.SetActive(false);
        loadchar1();
        loadchar2();
    }
    public void loadgamescene1()
    {
        PlayerPrefs.SetInt("selectedcharcter", 1);
        SceneManager.LoadScene(1);
    }

    public void loadgamescene2()
    {
        PlayerPrefs.SetInt("selectedcharcter", 2);
        SceneManager.LoadScene(1);
       
    }

}
